from selenium import webdriver
import time
from selenium.webdriver.common.keys import Keys

# Ensure user types input within apostrophes.
try:
    # Check if we have user's first time using app.
    f = open('details','r')
    username = f.readline()

    if username == "":
        phrase = "Hello new user, this is an automated Twitter emoji tweeter."
        for c in phrase: 
            print(c, end = '')
            time.sleep(.025)
        print()
        print()

        # Ask for account info.
        username = input("Enter your account username (in apostrophes): ")
        password = input("Enter your account password (in apostrophes): ")
        f = open('details','w')
        f.write(username + '\n' + password)
    
    password = f.readline()

    # Ask for emoji number
    num1 = str(input("Enter the row number (1-41): "))
    num2 = str(input("Enter the column number (1-9): "))
    print("Please wait as we login and post.")

    # Open Twitter in Firefox.
    browser = webdriver.Firefox()
    browser.get("http://www.twitter.com")

    time.sleep(2)

    # Click the login button.
    login = browser.find_element_by_xpath('/html/body/div[1]/div/div[1]/div[1]/div[2]/div[2]/div/a[2]')
    login.click()

    time.sleep(2)

    # Enter username and password, then click the login button again.
    user = browser.find_element_by_xpath('/html/body/div[1]/div[2]/div/div/div[1]/form/fieldset/div[1]/input')
    user.send_keys(username)
    pw = browser.find_element_by_xpath('/html/body/div[1]/div[2]/div/div/div[1]/form/fieldset/div[2]/input')
    pw.send_keys(password)

    login = browser.find_element_by_xpath('/html/body/div[1]/div[2]/div/div/div[1]/form/div[2]/button')
    login.click()

    time.sleep(2)

    # Enter tweet and post.
    tweet = browser.find_element_by_xpath('/html/body/div[1]/div/div/div/main/div/div/div/div[1]/div/div[2]/div[2]/div[1]/div/div/div[2]/div[2]/div/div/div[1]/div[4]/div')
    tweet.click()
    time.sleep(1)

    posting = browser.find_element_by_xpath('/html/body/div[1]/div/div/div[1]/div/div/div[2]/div[3]/div/div/div/div[1]/div/div[3]/div/div[3]/div/div[2]/div/div[' + num1 + ']/div[' + num2 + ']/div/div')
    posting.click()
    time.sleep(1)

    body = browser.find_element_by_xpath('/html/body/div[1]/div/div/div[1]/div/div/div[2]/div[1]')
    body.click()
    time.sleep(1)

    send = browser.find_element_by_xpath('/html/body/div[1]/div/div/div/main/div/div/div/div[1]/div/div[2]/div[2]/div[1]/div/div/div[2]/div[2]/div/div/div[2]/div[3]/div')
    send.click()
    time.sleep(1)

    browser.close()

except NameError:
    print("You may need to enter input with apostrophes (e.g. 'post' or 'snorlax123').")